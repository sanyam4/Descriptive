import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MLNComponent } from './mln.component';

describe('MLNComponent', () => {
  let component: MLNComponent;
  let fixture: ComponentFixture<MLNComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MLNComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MLNComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
