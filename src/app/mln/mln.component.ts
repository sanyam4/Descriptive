import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-mln',
  templateUrl: './mln.component.html',
  styleUrls: ['./mln.component.css']
})
export class MLNComponent implements OnInit {

  @Input() public myInput:any;
  @Output() myOutput:EventEmitter<string>=new EventEmitter();
  outputstring =" Hello!! Task Accompalished";

  constructor() { }

  ngOnInit(): void {
    console.log(this.myInput);
  }
  Datasend(){
    this.myOutput.emit(this.outputstring);
  }

}
