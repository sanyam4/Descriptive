import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MLNComponent } from './mln/mln.component';
import { SanyamComponent } from './sanyam/sanyam.component';

@NgModule({
  declarations: [
    AppComponent,
    MLNComponent,
    SanyamComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
