import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-sanyam',
  templateUrl: './sanyam.component.html',
  styleUrls: ['./sanyam.component.css']
})
export class SanyamComponent implements OnInit {
  
  formname={
    parentData:"Data is Passed form parent to child",
    name:'Sanyam',
    Age:'21',
    Gender:'Male',
    Designation:'Angular'
  }

  constructor() { }

  ngOnInit(): void {
  }

  Getdata(value:any)
    {
      console.log(value);
      alert(value);

    }
}
