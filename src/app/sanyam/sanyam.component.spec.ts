import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SanyamComponent } from './sanyam.component';

describe('SanyamComponent', () => {
  let component: SanyamComponent;
  let fixture: ComponentFixture<SanyamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SanyamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SanyamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
